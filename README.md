This repository comprises the data of all analyzed questions for the VaMoS 2019 submission "Hey, are You Talking about Software Product Lines? An Analysis of Developer Communities".

The zip folder comprises html websites that can be opened without internet connection and are directly linked to the corresponding folder.